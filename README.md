# circe-helper

## Build

Development build for your current platform:
    
    go build circe.go

Release build For windows:
   
    GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" circe.go

Release build for MacOS:

    GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" circe.go

Release build for Linux:

    GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" circe.go

(Optional) Binary compression, if UPX is available:

    upx circe
    
## Usage

	./circe [optional flags] source_file transformation_name destination_file 

Example:

	./circe test1.html html2pdf test1.pdf

Optional flags:

	-endpoint=[circe service endpoint]  # will default to env var CIRCE_ENDPOINT
	-appuuid=[your application UUID]  # will default to env var CIRCE_APP_UUID
	-secret=[your application secret]  # will default to env var CIRCE_SECRET